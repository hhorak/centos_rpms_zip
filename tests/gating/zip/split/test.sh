#!/bin/bash
# Copyright (C) 2019  Jakub Martisko <jamartis at redhat dot com>

# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:

#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

files="./in ./exp ./out.zip ./out.z01 ./out.z02 ./out2.zip"
rm -fr $files

for i in {1..100000}
do
    echo "Hello World" >> ./in
    echo "Hello World" >> ./exp
done

zip -Z store -s 500k ./out.zip ./in >/dev/null

rm -f ./in

ls ./out.z01 >/dev/null|| exit 1
ls ./out.z02 >/dev/null|| exit 1
ls ./out.zip >/dev/null|| exit 1



zip -o -s- ./out.zip -O ./out2.zip >/dev/null || exit 1

unzip ./out2.zip >/dev/null || exit 1
diff ./in ./exp || exit 1

rm -f $files
exit 0
